=================================================================================
Canon CAPT Printer Driver for Linux Version 2.20

PLEASE READ THIS DOCUMENT CAREFULLY
=================================================================================


---------------------------------------------------------------------------------
Trademarks

Adobe, Acrobat, Acrobat Reader, PostScript and PostScript 3 are trademarks of
Adobe Systems Incorporated.
Linux is a trademark of Linus Torvalds.
HP-GL is a trademark of Hewlett-Packard Company.
UNIX is a trademark of The Open Group in the United States and other countries.
Other product and company names herein may be the trademarks of their respective
owners.
---------------------------------------------------------------------------------


---------------------------------------------------------------------------------
CONTENTS

Before Starting
1. Introduction
2. Distribution File Structure of the Canon CAPT Printer Driver for Linux
3. Hardware Requirements
4. Auto startup setting procedure for ccpd daemon
5. Cautions, Limitations, and Restrictions
---------------------------------------------------------------------------------


1. Introduction -----------------------------------------------------------------
Thank you for using the Canon CAPT Printer Driver for Linux. This CAPT printer
driver provides printing functions for Canon LBP printers operating under
the CUPS (Common UNIX Printing System) environment, a printing system that
operates on Linux operating systems.


2. Distribution File Structure of the Canon CAPT Printer Driver for Linux -------
The Canon CAPT Printer Driver for Linux distribution files are as follows:
Furthermore, the file name for the CUPS driver common module and printer
driver module differs depending on the version.

- README-capt-2.2xE.txt (This document)
Describes supplementary information on the Canon CAPT Printer Driver for Linux.

- LICENSE-captdrv-2.2xE.txt
Describes User License Agreement on the Canon CAPT Printer Driver for Linux.

- guide-capt-2.2xE.tar.gz
Online manual that explains how to use the Canon CAPT Printer Driver for Linux.
This includes the system requirements, installation, and usage of the Canon CAPT
Printer Driver for Linux.
Because this file is in a compressed format, you need to extract it to the
appropriate directory before reading.

- cndrvcups-common-2.20-x.i386.rpm (for 32-bit)
- cndrvcups-common-2.20-x.x86_64.rpm (for 64-bit)
- cndrvcups-common_2.20-x_i386.deb (for Debian 32-bit)
Installation package for the CUPS driver common module used by the Canon CAPT
Printer Driver for Linux.

- cndrvcups-capt-2.20-x.i386.rpm (for 32-bit)
- cndrvcups-capt-2.20-x.x86_64.rpm (for 64-bit)
- cndrvcups-capt_2.20-x_i386.deb (for Debian 32-bit)
Installation package for the Canon CAPT Printer Driver for Linux.

- cndrvcups-common-2.20-x.tar.gz
Source file for the CUPS driver common module used by the Canon CAPT Printer
Driver for Linux.

- cndrvcups-capt-2.20-x.tar.gz
Source file for the Canon CAPT Printer Driver for Linux.


3. Hardware Requirements --------------------------------------------------------
This printer driver can be used with the following hardware environment.

Hardware:
    Computer that is enable to operate Linux, with x86 compatible CPU
    (32-bit or 64-bit)

Evaluated OS:
    Fedora 12 32-bit/64-bit
    Fedora 13 32-bit/64-bit
    Ubuntu 9.10 Desktop 32-bit
    Ubuntu 10.04 Desktop 32-bit

Previously Evaluated OS:
    Turbolinux 10 Desktop 32-bit
    Turbolinux 10 F... 32-bit
    Turbolinux 10 S 32-bit
    Turbolinux Version 11 FUJI 32-bit
    Turbolinux Home 32-bit
    MIRACLE LINUX V3.0(Asianux Inside) 32-bit
    MIRACLE LINUX V4.0(Asianux Inside) 32-bit
    Red Hat 9 32-bit
    Red Hat Professional Workstation 32-bit
    Red Hat Enterprise Linux v.4 32-bit
    Red Hat Enterprise Linux v.5 32-bit
    Mandriva Linux One 2008* 1 32-bit
    Mandriva Linux Powerpack 2008 32-bit
    Mandriva Linux One 2008 Spring 32-bit
    Mandriva Linux One 2009.0 32-bit
    SUSE LINUX PROFESSIONAL 9.3 32-bit
    Novell Linux Desktop 9 32-bit
    SUSE Linux 10.0 (openSUSE) 32-bit
    SUSE Linux 10.1 (openSUSE) 32-bit
    SUSE Linux 10.2 (openSUSE) 32-bit
    SUSE Linux 10.3 (openSUSE) 32-bit
    SUSE Linux 11.0 (openSUSE) 32-bit
    SUSE Linux 11.1 (openSUSE) 32-bit
    Fedora Core 4 32-bit
    Fedora Core 5 32-bit
    Fedora Core 6 32-bit
    Fedora 7 32-bit
    Fedora 8 32-bit
    Fedora 9 32-bit
    Fedora 10 32-bit
    Fedora 11 32-bit
    Ubuntu 7.04 Desktop 32-bit
    Ubuntu 7.10 Desktop 32-bit
    Ubuntu 8.04 Desktop 32-bit
    Ubuntu 8.10 Desktop 32-bit
    Ubuntu 9.04 Desktop 32-bit
    Debian GNU/Linux 3.1 rev2 32-bit
    Debian GNU/Linux 4.0 32-bit
    Debian GNU/Linux 4.0 r6 etchnhalf 32-bit
    Debian GNU/Linux 5.02 32-bit
    Vine Linux 3.1/3.1CR 32-bit
    Vine Linux 4.1 32-bit
    Vine Linux 4.2 32-bit
    Cent OS 5.3

Object Printer:
    LBP9100Cdn
    LBP7200C series
    LBP6300dn
    LBP6018/LBP6000
    LBP5300
    LBP5100
    LBP5050 series
    LBP5000
    LBP3500
    LBP3310
    LBP3300
    LBP3250
    LBP3210
    LBP3200
    LBP3150/LBP3108/LBP3100
    LBP3050/LBP3018/LBP3010
    LBP3000
    LBP2900
    LBP-1210
    LBP-1120

Please see the online manual about the install method and the concrete usage.


4. Auto startup setting procedure for ccpd daemon -------------------------------
When setting the Status Monitor to start automatically, ccpd daemon must be set
to start automatically as well.
Set ccpd daemon to start automatically in the following procedure.

<For a distribution with a /etc/rc.local file>
Log in as 'root' and add the '/etc/init.d/ccpd start' command to the
/etc/rc.local file.

<For a distribution with a /sbin/insserv command>
Log in as 'root', add the following comments to the third line in
/etc/init.d/ccpd, and execute the 'insserv ccpd' command.

### BEGIN INIT INFO
# Provides:          ccpd
# Required-Start:    $local_fs $remote_fs $syslog $network $named
# Should-Start:      $ALL
# Required-Stop:     $syslog $remote_fs
# Default-Start:     3 5
# Default-Stop:      0 1 2 6
# Description:       Start Canon Printer Daemon for CUPS
### END INIT INFO


5. Cautions, Limitations, and Restrictions --------------------------------------

- Ghostscript which includes common API is required to use this printer driver.
  Make sure to install Ghostscript before installing this printer driver.
  Refer to the following URL to find out how to get Ghostscript.
  http://opfc.sourceforge.jp/index.html.en

- If you install "cndrvcups-common" package version 2.20, make sure you install
  the same version of the "cndrvcups-capt" package, i.e. 2.20.

- When specifying multiple pages/copies for [Page Layout] in the [General]
  sheet to print a document created with StarSuite7/OpenOffice, due to a cause
  of operation by the CUPS module, settings are not correctly assigned to the
  multiple pages and output.

- PostScript files created with the number of copies specified in OpenOffice.org
  or StarSuite are affected not by the value specified by [Copies] in the 
  [cngplp] dialog box (the driver UI), but by the number of copies set when 
  creating the PostScript file.

- If settings are changed from the driver UI, during print processing, the
  printed result will reflect the changed settings.

- If [Brightness and Gamma] is specified in the [General] sheet from an
  application such as OpenOffice.org, GIMP, or Acrobat Reader v.5.0, the settings
  will be invalid.

- You cannot print a PDF document by directly specifying it from the desktop or
  command line. When printing a PDF document, it is recommended that you print
  it from Acrobat Reader or Adobe Reader.

- The maximum number of files that can be held in the print queue when printing
  is 500 according to CUPS specifications. Files queued after the 500th file will
  be ignored.

- When the paper size is specified from the application, the specified contents
  will not be valid. The value specified for [Page Size] in the [General] sheet
  in the printer driver screen will be reflected when printing.

- If you are using SUSE LINUX Professional 9.3, the driver UI may display
  unintelligible characters. You can solve this problem using the following
  method.
  1) Log in as 'root'.
  2) Execute the following command to change the GTK+ environment settings.
     # cd /etc/
     # ln -s opt/gnome/gtk ./

- If you are using SUSE LINUX Professional 9.3, a warning may occur when you activate
  the driver UI. You can solve this problem using the following method.
  1) Open [K Menu] -> [Control Center].
  2) Select [Appearance & Themes].
  3) Select [Colors].
  4) Deselect [Apply colors to non-KDE applications].
  5) Close [Control Center].

- If you are using LBP9100Cdn, LBP7200 series, LBP6300dn, LBP6018, LBP6000, 
  LBP5300, LBP5100, LBP5050 series, LBP5000, LBP3500, LBP3310, LBP3300, 
  LBP3250, LBP3150, LBP3108, LBP3100, LBP3050, LBP3018, or LBP3010, blank paper
  is not output according to Glue Code specifications, even if the print job 
  includes blank pages.
  The setting does not become effective even after setting [Use Skip Blank
  Pages Mode] in the [Finishing Details] dialog box to [Off] or specifying the 
  printer setting not to use the Skip Blank Pages mode from the command line.

- If you are using LBP5300 in a network environment, the version of the firmware
  of the network board needs to be 1.10 or later, otherwise the network board
  does not operate properly.
  Download the latest update file from the Canon website and update the firmware.

- If you are using LBP5000, LBP3500 or LBP3300 with NB-C1 installed in a network
  environment, the version of the firmware of the network board needs to be 1.30
  or later, otherwise the network board does not operate properly.
  Download the latest update file from the Canon website and update the firmware.

- If you are using LBP3310 with NB-C2 installed in a network environment, the
  version of the firmware of the network board needs to be 1.30 or later,
  otherwise the network board may not operate properly. Download the latest
  update file from the Canon website and update the firmware.

- If you use the Cancel Job key on the printer unit to cancel jobs, the jobs from
  CUPS may not be cleared depending on the data size. In this case, delete the
  jobs using the CUPS interface.

- If you cannot browse the IP address of localhost, you cannot start Status
  Monitor.
  Modify "/etc/hosts" so that you can browse the IP address of localhost.

- If you have connected the printer using a USB cable in the environment in which
  the HAL daemon runs on Fedora Core, delete the registered printer entry created
  by the HAL daemon with the printer name once, and then perform the registration
  operation for the printer.

- If you are using CUPS 1.2.x for the printing system, the printer may not 
  operate properly when printing banner pages.
  Before you print banner pages, replace the printing system with CUPS 1.1.x.

- If you are using the following distributions, you may not be able to install
  the printer driver successfully because "libstdc++.so.5" is not installed by
  default.
  In this case, perform the additional installation of the package.
  - For Fedora Core 4, Fedora Core 5, Fedora Core 6, Fedora 7, Fedora 8, 
    Fedora 9, Fedora 10, Fedora 11, Fedora 12, Fedora 13, 
    RedHat Enterprise Linux 5.1, and CentOS 5.3
    -> Install the package (compat-libstdc++-33).
  - For Ubuntu 7.10, Ubuntu 8.04, Ubuntu 8.10, Ubuntu 9.04, Ubuntu 9.10, 
    Ubuntu 10.04, Debian GNU/Linux 4.0, and Debian GNU/Linux 5.0
    -> Obtain the package (gcc-3.3-base or libstdc++5) from
       "http://lug.mtu.edu/ubuntu/pool/main/g/gcc-3.3/" to install it.
       Reference Information: http://ubuntuforums.org/showthread.php?t=674100
  - For Mandriva 2008 and Mandriva 2008 Spring
    -> Obtain the package (libstdc++5) from the standard mirror server etc. to
       install it.
  - For OpenSUSE 11.1
    -> Install the package (libstdc++33-.3.3-7.5.i586).

- If you are using SUSE Linux 9.3 or SUSE Linux 10.0, and are printing from
  the [Print] dialog box of Mozilla or FireFox, because the multiple copies
  setting is not enabled, you can print only one copy regardless of how many
  copies you have specified.
  This problem can be solved by changing the following line in the file
  "/etc/cups/mime.convs".
  [Before change]
    application/mozilla-ps application/postscript 33 pswrite
  [After change]
    application/mozilla-ps application/postscript 33 pstops

- When performing banner printing in Fedora 8 or Fedora 9, if you specify a
  setting other than [none] for [End] under [Banner] in the [General] sheet, 
  the print queue will stop.

- If you are using Fedora 9, restarting the CCPD daemon may stop the kernel.
  You can avoid this problem by updating the kernel to 
  "kernel-2.6.25.14-108.fc9".

- If you are using Fedora 11 and print with the print queue stopped after
  canceling a job, the job is suspended. In this case, click the [Maintenance]
  button in Printers in the CUPSWeb interface and select [Resume Printer] to
  perform the [pending since] job again.
  If you cannot find the [Maintenance] button, you can select [Resume Printer]
  by selecting [Pause Printer].

- If you are using OpenSUSE 10.2 or SLED10SP1, which includes Ghostscript
  version 8.15.3, you may not be able to print some documents. To solve this
  problem, install another version of Ghostscript.

- If you are using OpenSUSE 11.0 with Ghostscript version 8.6.x, printing from
  Evince, GIMP, or other applications may take time.

- If you are using Adobe Reader 7.0.x, and modify such settings as Paper Size,
  Paper Source, Duplex Printing, etc. in the print dialog window, these options
  are automatically added to the printer command. However, these settings will
  not work because they cannot be recognized as command options. To solve this
  problem, use "-o" to separate each command options.
     [before] -o InputSlot=Manual,Duplex=DuplexNotumble
     [after]  -o InputSlot=Manual -o Duplex=DuplexNoTumble

- When printing PDF files using Adobe Reader 8, there may be instances where 
  some image data is not printed.
  This problem may be solved by printing using Adobe Reader 7 or Adobe Reader 9,
  or setting Level 3 in the PostScript options.

- When performing 2-sided printing with Adobe Reader 8.1.2, if you specify
  [ON (Short-edged Binding)] for [Duplex Printing] in the print properties for
  Adobe Reader 8.1.2, the document will be printed on both sides with long-edged
  binding.
  This problem can be avoided by printing the document using the printer driver 
  UI.

- If you are using Vine Linux 3.1, you may take time to print from Adobe Reader
  7.0.9 or may not be able to print some documents.

- When printing PDF files containing Japanese characters from the command line
  in Vine Linux 4.1, there may be instances where Ghostscript terminates 
  unexpectedly, causing printing to stop.
  This problem can be avoided by printing PDF files using Adobe Reader.

- When printing PDF files from Adobe Reader 8 in Vine Linux 4.1, there may be 
  instances where Ghostscript terminates unexpectedly, causing the print queue 
  to stop.
  This is caused by Ghostscript (7.07) not being able to analyze PS files 
  created by Adobe Reader 8, and consequently terminating prematurely, thereby 
  stopping the filtering process.
  This problem can be avoided by using Adobe Reader 7.

- When printing text files in landscape orientation in Vine Linux 4.1, Vine Linux
  4.2, Fedora 8, Fedora 9, or RedHat Enterprise Linux v.5, there may be instances
  where the text file is printed in portrait orientation with some of the print
  data not being printed on the page. This is caused by the CUPS filter employed
  by the distribution you are using creating a PS command that is already set to
  portrait.
  Also, some of the functions provided in the CUPS standard filter "texttops"
  may not operate correctly.
  This problem can be avoided by changing the CUPS filter name specified in the
  "text/plain" entry line in the CUPS setting file "mime.convs" to the CUPS
  standard filter "texttops". This will result in Japanese characters being
  misprinted, therefore when printing Japanese characters, it is necessary to
  print a PS command created with a text editor or text/PostScript conversion
  program such as paps.

- If you specify Paper Source settings in the print dialog of an application
  such as Writer of OpenOffice.org, the settings made from the application are
  overridden by the printer driver UI settings. To print from the desired
  Paper Source, specify the Paper Source from the printer driver UI beforehand,
  or print from the command line.

- If you are using Debian GNU/Linux 4.0, a PPD file error may occour when you
  register the printer (PPD) with the print spooler. To solve this problem,
  use "-P (full path to the ppd)" instead of "-m" when you specify the ppd
  using the command line.
    Example: /usr/sbin/lpadmin -p LBP5000
              -P /usr/share/cups/model/CNCUPSLBP5000CAPTK.ppd
              -v ccp://localhost:59687 -E

- If you are using Debian GNU/Linux 4.0 r6, and attempt to print a text file 
  using the printer driver UI when EUC-JP is set as the locale, printing will 
  fail.
  This problem can be solved by printing a PS command created with a text editor
  or text/Postscript conversion program such as paps.

- If you are using Debian GNU/Linux 5.0.2, the gs-esp module is required to
  install the common module.
  You can install the gs-esp module by executing the following command.
    # apt-get install gs-esp

- If you are using Ubuntu 7.10, you may fail to print because it introduces
  security software called AppArmor.
  As the workaround, disable the AppArmor CUPS profile by running
  sudo aa-complain cupsd to enable printing.
  For more details, see Ubuntu 7.10 Release Notes.

- If you are using Ubuntu 8.10, Ubuntu 9.04, Ubuntu 9.10, and Ubuntu 10.04, the
  printer will print with the default paper output method, regardless of whether
  you have specified the paper output method.
  This problem can be solved by changing the output paper method setting from 
  the CUPS printer settings (Web).

- If you are using Ubuntu 8.10, specifying reverse order for printing does not 
  affect the print result.
  This problem can be solved by updating CUPS.

- If you are using Ubuntu Ubuntu 8.10, Ubuntu 9.04, Ubuntu 9.10, Ubuntu 10.04, 
  when printing PDF data or PS data, brightness and gamma correction settings 
  do not affect the print result.

- If you are using Ubuntu 7.04, Ubuntu 7.10, Ubuntu 8.04, Ubuntu 8.10, 
  Ubuntu 9.04, Debian GNU/Linux 3.1, Debian GNU/Linux 4.0, or 
  Debian GNU/Linux 5.0, the libcupsys2 library is required to install 
  the common module.
  You can install the libcupsys2 library by executing the following command.
    # apt-get install libcupsys2

- If you are using Ubuntu 9.04 and update the CUPS version to 
  "1.3.9-17ubuntu3.2", printing will fail due to improper PS data.
  You can avoid this problem by downgrading the CUPS version to 
  "1.3.9-17ubuntu3.1".
  <Downgrading with the apt-get command>
    - Execute the following command.
        # apt-get install cups=1.3.9-17ubuntu3.1
  <Downloading the CUPS1.3.9-17ubuntu3.1 package and downgrading with the dpkg
  command>
    - Download the package (cups_1.3.9-17ubuntu3.1_i386.deb) from the Ubuntu 
      website (http://packages.ubuntu.com/en/jaunty/i386/cups/download) using
      a Web browser etc., and then execute the following command.
        # dpkg -i cups_1.3.9-17ubuntu3.1_i386.deb

- If you are using Ubuntu 9.04, Ubuntu 9.10, Ubuntu 10.04, Fedora 11, Fedora 12
  or Fedora 13, and print banner pages, the specified number of banner pages 
  are printed.

- When printing PDF files from Adobe Reader in Mandriva, regardless of the
  version being used, there may be instances where Ghostscript terminates 
  unexpectedly, causing the print queue to stop.
  This is caused by Ghostscript (8.60) not being able to analyze PS commands 
  created using PS files for which security settings have been specified, and 
  consequently terminating prematurely, thereby stopping the filtering process.
  This problem can be avoided by not printing PDF files that have security 
  settings using Adobe Reader.

- If you are using Mandriva One 2008 Spring or Mandriva 2008 PowerPack with CUPS
  version 1.3.6, unintended print results may occur even when printing with
  standard CUPS print functions.
  This problem can be solved by updating CUPS.

- If you want to use multiple printers with the same device path (such as
  /dev/usb/lp0) on a USB connection, exit the ccpd daemon, and then add a
  printer when you switch the connection.
  If you do not exit the ccpd daemon, you may not be able to obtain appropriate
  printout results.

- If 15 or more characters are specified for the host name of Linux, the printer
  status may not display properly in Status Monitor.

- If you are using printers of the same model with one computer, and one is
  connected via USB and another is connected via network, the printer status may
  not display properly in Status Monitor.

- If your version of Ghostscript is 8.6x, you may not be able to print some
  documents.

- If you are using CentOS 5.3, you cannot print the number of copies as you 
  specified in Evidence.
  You can solve this problem by printing from other PDF viewers such as 
  Adobe Reader or using the following methods.
  1) Set the number of copies to 1 and select a PS command for the output 
     destination in Evince to output a file.
  2) Print the PS command output as a file after specifying the number of 
     copies in cngplp.

- When updating and installing the printer driver from version 1.90 to version
  2.20, you need to re-register the printer registered before updating and 
  installing the printer driver.

- When updating and installing the printer driver of the deb package from 
  version 1.90 or earlier to version 2.20, the message to check if you install
  "ccpd.conf" included in the installation package may be displayed in the 
  console.
  In this case, install "ccpd.conf" included in the installation package.

- Depending on the version of GTK (GIMP Toolkit), some characters may be
  unintelligible when displayed on the screen, but this does not indicate
  a problem with the functions and values set. Redraw the corresponding text
  area to solve this problem.

- If you are using Fedora 12 or Ubuntu 9.10, when you change the default options
  from the CUPS Web interface, the default values will be saved even if there is
  a conflict between the settings for each function. Also, once the settings are
  saved with a conflict, you cannot save the settings again even if you use the
  Web interface to change them to the correct values where there is no conflict.
  If you display the [cngplp] dialog box in this situation, an invalid operation
  may occur.
  You can use the following methods to solve this problem:
    [Method 1] Fedora 12 (32-bit/64-bit) and Ubuntu 9.10
      Re-register the printer that performed the invalid operation.
    [Method 2] Fedora 12 (32-bit/64-bit)
      Execute the following command to update CUPS:
    <For Fedora 12 (32-bit)> # yum update cups.i686
    <For Fedora 12 (64-bit)> # yum update cups.x86_64

- To install the common module in the 64-bit version of Fedora 12, or 
  Fedora 13, you need the 32-bit version glibc library.
  You can install the glibc library by executing the following command:
    # yum install glibc.i686
  Also, printer drivers from version 2.20 onward require the 32-bit version of 
  the gcc, stdc, popt, and xml2 library.
  You can enable printing by executing the following command:
    # yum install libgcc.i686
    # yum install libstdc++.i686
    # yum install popt.i686
    # yum install libxml2.i686

- If you are using the 32 bit or 64 bit version of Fedora 13, and print a TIFF 
  or JPEG file from the driver UI or command line, the printed image may be 
  broken up. 
  This problem can be solved by outputting the file as a PostScript file from an 
  application such as GIMP, then printing from the command line by typing the 
  PostScript command used to output the file after [cngplp].

- If you are using Fedora 12 or Fedora 13, even if you specify the brightness 
  and gamma settings from the driver UI or the command line , these settings 
  are not applied to the printed result from the second page onward. 
  This is due to these functions not being enabled because Ghostscript does not 
  correctly recognize the PostScript data created by the application.


=================================================================================
Support
=================================================================================
This Software and Related Information are independently developed by Canon and
distributed by Canon local company. Canon as manufacturer of printers supporting
the Software and Related Information ("Canon Printers"), and Canon local company
as selling agency of Canon Printers, do not receive any user support call and
/or request or any requests for information about the Software and Related
Information. Any demand for information about Canon Printers including
information about the repair and supplies for Canon Printers should be directed 
to Canon local company.
=================================================================================
                                                        Copyright CANON INC. 2010
